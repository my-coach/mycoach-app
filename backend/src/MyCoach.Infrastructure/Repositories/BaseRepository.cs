using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyCoach.Infrastructure.Data;

namespace MyCoach.Infrastructure.Repositories
{
    public class BaseRepository<TKey, TValue> : IRepository<TKey, TValue> where TValue : class
    {
        private readonly AppDbContext _context;

        private readonly DbSet<TValue> _entity;

        public BaseRepository(AppDbContext context)
        {
            _context = context;
            _entity = context.Set<TValue>();
        }

        public async Task<TValue> GetAsync(TKey id)
        {
            return await _entity.FindAsync(id);
        }

        public async Task CreateAsync(TValue entity)
        {
            _context.Entry(entity).State = EntityState.Added;

            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(TKey key)
        {
            var entity = await GetAsync(key);
            
            _context.Entry(entity).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(TValue entity)
        {
            _context.Entry(entity).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(TValue entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}