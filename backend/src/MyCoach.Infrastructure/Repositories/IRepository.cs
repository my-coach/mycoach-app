using System.Threading.Tasks;

namespace MyCoach.Infrastructure.Repositories
{
    public interface IRepository<in TKey, TEntity> where TEntity : class
    {
        Task<TEntity> GetAsync(TKey id);
        Task CreateAsync(TEntity entity);
        Task DeleteAsync(TKey key);
        Task DeleteAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
    }
}